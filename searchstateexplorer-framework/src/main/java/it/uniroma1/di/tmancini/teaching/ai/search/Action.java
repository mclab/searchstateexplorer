package it.uniroma1.di.tmancini.teaching.ai.search;

public abstract class Action { 

	public abstract double getCost();
}